package main

import (
    "flag"
    "os"
    "path/filepath"
    "strings"
    "net/mail"
    "mime"
    "fmt"
    notify "github.com/mqu/go-notify"
)

var DIR_MAIL string = os.Getenv("HOME")+"/mail"

var flag_l = flag.Bool("l", false, "Check local maildir")

func init() {
    flag.Usage = func() {
        fmt.Fprintf(os.Stderr, "Usage: %s [-l]\n", os.Args[0])
        flag.PrintDefaults()
        os.Exit(1)
    }
}

func main() {
    flag.Parse()

    if *flag_l {
        chklocal()
    } else {
        flag.Usage()
    }
}

func chklocal() {
    var (
        n int
        msg string
    )

    f, _ := filepath.Glob(DIR_MAIL+"/*/*/new/*")

    for i :=0; i < len(f); i++ {
        if strings.Contains(f[i], "/work/sent") {
            continue
        }

        //fmt.Println(f[i])

        r, _ := os.Open(f[i])
        m, _ := mail.ReadMessage(r)

        dec := new(mime.WordDecoder)
        subj, _ := dec.DecodeHeader(m.Header.Get("Subject"))
        from, _ := mail.ParseAddress(m.Header.Get("From"))
        dt, _ := mail.ParseDate(m.Header.Get("Date"))
        sdt := dt.Format("2006-01-02 15:04")

        msg += "\n"+sdt+"\n"+from.Name+" &lt;"+from.Address+"&gt;\n"
        msg += "<b>"+subj+"</b>\n"

        n++
    }

    if n == 0 {
        return
    }

    fmt.Println(n);
    noti("New mail", msg)
}

func noti(title, msg string) {
    notify.Init("chklocal")
    defer notify.UnInit()

    noti := notify.NotificationNew(title, msg, "dialog-information")
    if noti == nil {
        fmt.Fprintf(os.Stderr, "Unable to create a new notification\n")
        return
    }

    if e := notify.NotificationShow(noti); e != nil {
        fmt.Fprintf(os.Stderr, "%s\n", e.Message())
        return
    }

    notify.NotificationClose(noti)
}
